import React from 'react'
import ReactDOM from 'react-dom'
import { BrowserRouter, Route, Link } from 'react-router-dom'

import { Main, Main1, Main2 } from './comps'

const Main55 = ({ routes }) => (
  <div>
    <h2>Main With SubRoutes</h2>
    <Link to="/m/main1">Main1</Link>
    <Link to="/m/main2">Main2</Link>
    {routes.map((r, i) => <WithSubRoutes key={i} {...r} />)}
  </div>
)

const routes = [
  {
    path: '/main',
    component: Main
  },
  {
    path: '/m',
    component: Main55,
    routes: [
      {
        path: '/m/main1',
        component: Main1
      },
      {
        path: '/m/main2',
        component: Main2
      }
    ]
  }
]

const WithSubRoutes = ({ component: Component, path, routes, ...rest }) => (
  <Route
    path={path}
    {...rest}
    render={props => <Component {...props} routes={routes} />}
  />
)

const App = () => (
  <div>
    <Link to="/m">Sub MAin </Link>
    <Link to="/main">Main</Link>
    {routes.map((r, i) => <WithSubRoutes key={i} {...r} />)}
  </div>
)

ReactDOM.render(
  <BrowserRouter>
    <App />
  </BrowserRouter>,
  document.getElementById('root')
)
